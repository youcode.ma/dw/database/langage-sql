**===========lanage SQL=================**

Voilà un modèle relationnel de la base BDVol :

Avion (NumeroA,Compagnie,Constructeur,Modèle,Capacité)
Horaire (NumeroH,VilleDépart,VilleArrivée, HeureDépart, HeureArrivée)
Vol (NumeroV, JourSem, Jour, PlacesLibres, #Avion, #Horaire)

Nous avons ici trois relations nommées Avion, Horaire et Vol.
Pour distinguer les clés primaires et les clés étrangères, nous prendrons les conventions suivantes: les clefs primaires sont soulignées et les clefs étrangères sont précédées d'un #.

**I - Gestion des Passagers**

On voudrait ajouter une table de passagers à la base BDVols.
Un passager est décrit par son numéro, son nom et son prénom. Ces attributs seront respectivement
Représentés par :
• un champ NumeroP de type INT
• un champ Nom de type VARCHAR(20).
• un champ Prénom de type VARCHAR(20).
La table qui contient ces données se nomme Passagers.

**2) Adjonction des passagers**
Avec une seule requête INSERT INTO, ajoutez trois passagers dans la table de manière à obtenir:

NumeroP	      NOM	    PRENOM

1	         Thirion	Eric

2             Gaston	Lagaffe

3            Clapton	 Eric

**3) Correction de l'erreur à la deuxième ligne**

Dans la deuxième ligne, le nom et le prénom sont inversés. Corrigez cette erreur en utilisant les requêtes
DELETE et INSERT.

« 2   Lagaffe   Gaston »

**4) Modification des noms et des prénoms**

Sans utiliser les numéros des passagers et en utilisant uniquement la requête UPDATE, modifiez la table passager afin d'obtenir le résultat suivant:

NumeroP                Nom                Prenom 

1                    Thirion              Alex

2                      Deferre             Gaston 

3                      Clapton             Alex

================================================

**II - Exécution de requêtes**

**1 : Selection de colonnes**

Afficher une table contenant les colonnes NumeroV, Jour et PlacesLibres de la table Vols.

**2 : Selection de lignes**

Afficher une table contenant les lignes de la table Vols dont le jour de la semaine est Lundi.

**3 : Selection multicritère**

Afficher les lignes de la table Vols dont le jour de la semaine est Mardi, le nombre de places libres strictement supérieur à 150 et la date de départ antérieure au 11 septembre 2001.
Remarques :

• en MySQL les dates se notent entre guillemets dans le format aaaa-mm-jj. Par exemple
"1789-07-14" désigne le 14 Juillet 1789.

• pour comparer des dates, on peut utiliser les symboles habituellement utilisés pour comparer des nombres ( <, <= ,>, >=, =, <>)

**4 : Selection multi-tables**

**Exercice 1**

Afficher la jointure des trois tables Vols, Avions et horaires, en sélectionnant uniquement (dans cet ordre) la
ville de départ, la ville d'arrivée, le jour de la semaine, la date du départ, l'heure de départ, l'heure d'arrivée, la compagnie de transport, le constructeur et le modèle de l'avion.

**Exercice2**

Ecrire une requête qui affiche la jointure des tables Vols, Horaires, Avions en sélectionnant uniquement les
vols Paris-Londre avec au moins 20 places libres, partant avant le 11 septembre 2001, au plus tard à dix
heures, un lundi ou un mercredi. ( utiliser JourSem IN ('Lundi','Mercredi') ).

La table affichée ne doit contenir que les colonnes suivantes : le numéro du vol, le jour de la semaine, la
date du vol, le nombre de places libres, la ville de départ, la ville d'arrrivée, l'heure de départ, l'heure
d'arrivée et la compagnie de transport.

**5 : Elimination de doublons**

Ecrire une requête qui affiche tous les types d'avions (Modèle+Constructeur) de la base par une requête
SELECT appliquée à la table Avions.

**6 : Tri d'une table**

Ecrire une requête qui affiche les vols triés par la ville de départ, puis par la ville d'arrivée, puis par la date de départ, puis par l'heure de départ (ordre ascendant dans tous les cas).

**7 - Colonnes calculées**

Ecrire une requêtes donnant le nombre de places occupées de chaque vol. Le nom de cette nouvelle
colonne sera PlacesOccup. Les autres colonnes à afficher sont les colonnes existantes NumeroV, Avion, Capacite et PlacesLibres.

================================================================================

**III- EXERCICES THEORIQUES**

**1. Jointures **

La table T1 fait référence à la table T2 (clé primaire C3) , via sa clé étrangère C2 :

T1

| C1 | C2 |
| ------ | ------ |
| 1 | C | 
| 2 | A |
| 3 | D | 
| 4 | A | 

T2

| C3 | C4 | C5 |
| ------ | ------ | ------ |
| A | 10 | 15 |
| B | 0  | 15 |
| C | 30 | 20 |
| D | 50 | 10 |
| E | 20 | 60 |

Donnez le résultat de la requête suivante: SELECT C1, C4, C5 FROM T1, T2 WHERE C2 = C3

Quelles seraient les dimensions de la table obtenue en omettant la jointure C2 = C3 ?

==================**Procédures stockées**=================================

**Soit la base de données suivante :**

Stagiaire ( N°stagiaire, nom prénom, datenaiss, dateinscr, adresse, tel, #Nfiliere)
Filière ( Nfiliere, intitulfil, capacité , nbreannées)
Notation (N°notation, #N°stagiaire, N°module, note)
Module ( N°module, intitulemod, masse horaires)

1) donnez La procédure permettant de lister les stagiaires d'une filière donnée
2) donnez La procédure permettant d'afficher les stagiaires ayant l'âge dans la tranche précisé par l'utilisateur
3) Augmenter d'un point les notes des stagiaires dans le module -métier et formation
4) donnez La liste des stagiaires dont le nom commence par une lettre spécifiée par l'utilisateur
5) donnez Le bulletin de notes d'un stagiaire donné
6) donnez la Liste des stagiaires inscrits entre deux dates
7) Procédure qui supprime une filière avec l'ensemble des stagiaires affectés à cette filière



